package com.silverbars;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OrderServiceTest {

    OrderService orderService;
    OrderRepository inMemoryOrderRepository;

    @Before
    public void setup() {
        inMemoryOrderRepository = new InMemoryOrderRepository();
        orderService = new OrderService(inMemoryOrderRepository);
    }

    @Test
    public void should_register_an_order() {

        // given
        Order order = new Order("1234567890", 3500, BigDecimal.valueOf(303), OrderType.BUY);

        // when
        orderService.register(order);

        // then
        assertThat(inMemoryOrderRepository.getOrders().collect(toList()), is(singletonList(order)));
    }

    @Test
    public void should_cancel_a_registered_order() {

        // given
        Order order = new Order("1234567890", 3500, BigDecimal.valueOf(303), OrderType.BUY);

        // and
        inMemoryOrderRepository.create(order);
        assertThat(inMemoryOrderRepository.getOrders().collect(toList()), is(singletonList(order)));

        // when
        orderService.cancel(order);

        //then
        assertThat(inMemoryOrderRepository.getOrders().collect(toList()), is(EMPTY_LIST));
    }

    @Test
    public void should_return_sell_orders_info() {

        // given
        orderService.register(new Order("user1", 3500, BigDecimal.valueOf(306), OrderType.SELL));
        orderService.register(new Order("user2", 1200, BigDecimal.valueOf(310), OrderType.SELL));
        orderService.register(new Order("user3", 1500, BigDecimal.valueOf(307), OrderType.SELL));
        orderService.register(new Order("user4", 2000, BigDecimal.valueOf(306), OrderType.SELL));

        // when
        List<OrderInfo> sellOrders = orderService.getOrderSummary(OrderType.SELL);

        // then
        List<OrderInfo> expectedOrderList = Arrays.asList(
                new OrderInfo(5500, BigDecimal.valueOf(306)),
                new OrderInfo(1500, BigDecimal.valueOf(307)),
                new OrderInfo(1200, BigDecimal.valueOf(310))
        );
        assertThat(sellOrders, is(expectedOrderList));
    }

    @Test
    public void should_return_buy_orders_info() {

        // given
        orderService.register(new Order("user1", 3500, BigDecimal.valueOf(306), OrderType.BUY));
        orderService.register(new Order("user2", 1200, BigDecimal.valueOf(310), OrderType.BUY));
        orderService.register(new Order("user3", 1500, BigDecimal.valueOf(307), OrderType.BUY));
        orderService.register(new Order("user4", 2000, BigDecimal.valueOf(306), OrderType.BUY));

        // when
        List<OrderInfo> sellOrders = orderService.getOrderSummary(OrderType.BUY);

        // then
        List<OrderInfo> expectedOrderList = Arrays.asList(
                new OrderInfo(1200, BigDecimal.valueOf(310)),
                new OrderInfo(1500, BigDecimal.valueOf(307)),
                new OrderInfo(5500, BigDecimal.valueOf(306))

        );
        assertThat(sellOrders, is(expectedOrderList));
    }
}
