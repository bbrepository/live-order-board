package com.silverbars;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

public class OrderService {

    private OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void register(Order order) {
        orderRepository.create(order);
    }

    public void cancel(Order order) {
        orderRepository.remove(order);
    }

    public List<OrderInfo> getOrderSummary(OrderType orderType) {

        Map<BigDecimal, Integer> quantityPerPrice = orderRepository.getOrders(orderType)
                .collect(groupingBy(Order::getSterlingPoundsPerKg,
                        reducing(0, Order::getQuantityInGrams, Integer::sum)));

        return quantityPerPrice.entrySet().stream()
                .map(e -> new OrderInfo(e.getValue(), e.getKey()))
                .sorted(compareByPrice(orderType))
                .collect(toList());
    }

    private Comparator<OrderInfo> compareByPrice(OrderType orderType) {
        return (o1, o2) -> {
            switch (orderType) {
                case BUY:
                    return o2.getSterlingPoundsPerKg().compareTo(o1.getSterlingPoundsPerKg());
                case SELL:
                    return o1.getSterlingPoundsPerKg().compareTo(o2.getSterlingPoundsPerKg());
                default:
                    return 0;
            }
        };
    }
}
