package com.silverbars;

import java.util.stream.Stream;

/**
 * This interface is to allow different implementations for OrderRepository (e.g. some non in-memory implementation)
 */
public interface OrderRepository {

    boolean create(Order order);

    boolean remove(Order order);

    Stream<Order> getOrders();

    Stream<Order> getOrders(OrderType orderType);

}
