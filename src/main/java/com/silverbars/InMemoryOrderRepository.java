package com.silverbars;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class InMemoryOrderRepository implements OrderRepository {

    private static final Stream<Order> EMPTY_STREAM = Stream.empty();
    private List<Order> sellOrders = new LinkedList<>();
    private List<Order> buyOrders = new LinkedList<>();

    @Override
    public boolean create(Order order) {
        return byOrderType(order.getType(), l -> l.add(order)).orElse(false);
    }

    @Override
    public boolean remove(Order order) {
        return byOrderType(order.getType(), l -> l.remove(order)).orElse(false);
    }

    @Override
    public Stream<Order> getOrders() {
        return Stream.concat(sellOrders.stream(), buyOrders.stream());
    }

    @Override
    public Stream<Order> getOrders(OrderType orderType) {
        return byOrderType(orderType, Collection::stream).orElse(EMPTY_STREAM);
    }

    private <T> Optional<T> byOrderType(OrderType orderType, Function<List<Order>, T> fun) {
        switch (orderType) {
            case BUY:
                return Optional.ofNullable(fun.apply(buyOrders));
            case SELL:
                return Optional.ofNullable(fun.apply(sellOrders));
            default:
                return Optional.empty();
        }
    }
}
