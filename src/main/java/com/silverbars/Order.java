package com.silverbars;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;

public class Order {

    private String userId;
    private int quantityInGrams;
    private BigDecimal sterlingPoundsPerKg;
    private OrderType type;

    public Order(String userId, int quantityInGrams, BigDecimal sterlingPoundsPerKg, OrderType type) {
        this.userId = userId;
        this.quantityInGrams = quantityInGrams;
        this.sterlingPoundsPerKg = sterlingPoundsPerKg;
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public int getQuantityInGrams() {
        return quantityInGrams;
    }

    public BigDecimal getSterlingPoundsPerKg() {
        return sterlingPoundsPerKg;
    }

    public OrderType getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
