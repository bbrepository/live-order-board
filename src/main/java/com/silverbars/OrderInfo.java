package com.silverbars;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

public class OrderInfo {

    private int quantityInGrams;
    private BigDecimal sterlingPoundsPerKg;

    public OrderInfo(int quantityInGrams, BigDecimal sterlingPoundsPerKg) {
        this.quantityInGrams = quantityInGrams;
        this.sterlingPoundsPerKg = sterlingPoundsPerKg;
    }

    public int getQuantityInGrams() {
        return quantityInGrams;
    }

    public BigDecimal getSterlingPoundsPerKg() {
        return sterlingPoundsPerKg;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
